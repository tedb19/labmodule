import { seedTestLabOrders } from './laborder.seed'
import models from '../models'
import { log } from '../utils/log.utils'

export const initializeDb = async (options = { force: true }) => {
    return await models
            .sequelize
            .sync(options)
            .then(() => seedTestLabOrders())
            .then(() => log.info('All seed data uploaded...'))
            .catch(error => log.error(error))
}