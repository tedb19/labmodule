
import modem from 'modem'

const err = (message) => {
    console.log('No path to device specified!')
}

const safModem = modem.Modem()

export const readSMS = (device) => {
    if(!device) err()
        
    safModem.open(device, function() {
        safModem.getMessages(function() {
        console.log(arguments)
        })
    
        safModem.on('sms received', function(sms) {
        console.log(sms)
        })
    })
}
