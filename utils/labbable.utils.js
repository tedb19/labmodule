import Labbable from 'labbable'

export const labbablePluginObj = {
    register: Labbable.plugin
}
