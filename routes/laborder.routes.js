import Boom from 'boom'

import models from '../models'
import { log } from '../utils/log.utils'
import { createHash } from '../utils/hashing.utils'
import jsonxml from 'jsontoxml'

exports.register = (server, options, next) => {
    server.route({
        path: '/laborder',
        method: 'GET',
        handler: async (request, reply) => {            
            const labOrders = await models.LabOrder.findAll({ where: { transferFlag: false }})
            const newLabs = labOrders.map((labOrder, idx) => ({idx: labOrder}))
            reply(jsonxml(JSON.stringify(newLabs))).type('application/xml')
        },
        config: {
            cache: {
                expiresIn: 300 * 1000,
                privacy: 'private'
            },
            description: 'Get the lab orders',
            tags: ['lab order', 'lab orders'],
            notes: 'should return all the lab orders',
            cors: {
                origin: ['*'],
                additionalHeaders: ['cache-control', 'x-requested-with', 'application/xml']
            }
        }
    })

    server.route({
        path: '/laborder/processed',
        method: 'POST',
        handler: async (request, reply) => {            
            console.log(request.payload)
            //update transferFlag to true
            reply(true)
        },
        config: {
            cache: {
                expiresIn: 300 * 1000,
                privacy: 'private'
            },
            description: 'Returns the processed lab orders',
            tags: ['lab order', 'lab orders'],
            notes: 'should return all the lab orders',
            cors: {
                origin: ['*'],
                additionalHeaders: ['cache-control', 'x-requested-with']
            }
        }
    })

    server.route({
        path: '/labresults',
        method: 'POST',
        handler: async (request, reply) => {            
            console.log(request.payload)
            //update transferFlag to true
            reply(true)
        },
        config: {
            cache: {
                expiresIn: 300 * 1000,
                privacy: 'private'
            },
            description: 'Returns the processed lab orders',
            tags: ['lab order', 'lab orders'],
            notes: 'should return all the lab orders',
            cors: {
                origin: ['*'],
                additionalHeaders: ['cache-control', 'x-requested-with']
            }
        }
    })
    
    return next()
}

exports.register.attributes = {
    name: 'laborder.routes'
}