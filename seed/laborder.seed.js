import models from '../models'

export const seedTestLabOrders = () => models.LabOrder.bulkCreate(labOrders)

export const labOrders = [
    {id: 1, orderNumber: '12345', orderDateTime: "2017-08-08", patientID: 'ABC345', patientName: 'Rufus Njenga', gender: 'M', age: '34', ageUnit: 'YEARS', clinicianName: 'Teddy B', investigation: 'VL', testNumber: '1', receiptNumber: '87y7hh', requestTransferredDate: '2017-08-07'},
    {id: 2, orderNumber: '67890', orderDateTime: "2017-08-18", patientID: 'EFG345', patientName: 'Mwenda Stans', gender: 'M', age: '33', ageUnit: 'YEARS', clinicianName: 'Teddy B', investigation: 'VL', testNumber: '1', receiptNumber: '788yghh', requestTransferredDate: '2017-08-17'},
    {id: 3, orderNumber: '7890', orderDateTime: "2017-08-18", patientID: 'EFG345', patientName: 'Mwenda Stans', gender: 'M', age: '33', ageUnit: 'YEARS', clinicianName: 'Teddy B', investigation: 'VL', testNumber: '1', receiptNumber: 'vhgvh6', requestTransferredDate: '2017-08-17', transferFlag: true}
]
