import labOrderRoutes from './laborder.routes'

export const routesPlugins = [
    { register: labOrderRoutes }
]
