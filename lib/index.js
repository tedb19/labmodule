import Hapi from 'hapi'
import Labbable from 'labbable'

import { log } from '../utils/log.utils'
import { utilsPlugins } from '../utils'
import { routesPlugins } from '../routes'
import { initializeDb } from '../seed'
import { readSMS } from '../modem/readsms'

const server = module.exports = new Hapi.Server();
const device = 'USBCDCACM\\VID_12D1&SUBCLASS_02&PROT_03\\6&212A80F2&0&0002_00'

server.connection({ port: 3007 })

const plugins = [ ...utilsPlugins, ...routesPlugins ]

server.register(plugins, (error) => {
    if (error) throw error
    
    server.initialize((error) => {
        if (error) throw error
        initializeDb()
            .then(() => {
                server.start((error) => {
                    if (error) throw error
                    log.info(`Server started @ ${server.info.uri}`)
                    //readSMS(device)
                })
            })
            .catch(error => log.error(error))
    })
})